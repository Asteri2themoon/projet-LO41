#ifndef __ENVIRONMENT_H__
#define __ENVIRONMENT_H__

#include <sys/types.h>
#include <sys/ipc.h>
#include <pthread.h>

#include <building.h>
#include <people.h>

#define IPC_MESSAGE_MOVE 1
#define IPC_MESSAGE_ELEVATOR 2
#define IPC_MESSAGE_PATHFINDING 3

typedef struct{
	pthread_cond_t condition;
	pthread_mutex_t mutex;
}Monitor;

typedef struct{
	People *p;
	int targetRoom;
	unsigned int targetFloor;
	Monitor* m;
}ActionRequest;

void initEnvironment(char *path);
void freeEnvironment();

void sendAction(People *p,unsigned int newRoom,unsigned int newFloor);
void sendElevatorStat(Elevator *e);
void sendPathFinding(People *p);
void getMessage();
void updatePosition(Elevator **tab);

ActionRequest* popPeopleInRoom(unsigned int floor,int room,int targetElevator);

//void AIBuildState(Elevator *e,double *cost,unsigned int mask,int ti);
//unsigned int AICentroid(double *cost);
unsigned int getTarget(Elevator *e);
int AIPathFinding(Position *lst,unsigned int cFloor,int cRoom,unsigned int tFloor,int tRoom);

void updatePathOfClosestTech(Elevator *e);

Goal randomGoalHost();

#endif
