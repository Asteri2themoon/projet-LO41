#ifndef __ROOM_H__
#define __ROOM_H__

#include <stdbool.h>

#define ELEVATOR_COUNT 3
#define NO_ELEVATOR 0x0
#define ELEVATOR_0 0x1
#define ELEVATOR_1 0x2
#define ELEVATOR_2 0x4

#define ROOM_TYPE_COUNT 4
typedef enum{CORRIDOR,OFFICE,HALL,TECHROOM} RoomType;

typedef struct{
	RoomType type;
	unsigned int capacity;
	unsigned int nbPeople;
	bool elevator[3];
}Room;

Room* initRoom(RoomType type,unsigned int c,unsigned int elevator);
void freeRoom(Room *r);

//random type except corridor and hall
RoomType randRoomType();
char* roomType2Str(RoomType type);

#endif
