GXX=gcc
FILES=*.c
LIBS=-pthread -lm
PROJECT=projet_lo41
FLAGS=-Wall -Werror -I. -g
LOG=log.txt

all: clear build run printLog

build:
	$(GXX) $(FILES) $(FLAGS) $(LIBS) -o $(PROJECT)

run:
	@./$(PROJECT)

run-gui:
	@./$(PROJECT) | python3 display.py

clear:
	@rm -f $(PROJECT) *.o *.txt

stat:
	@cat *.c *.h | wc -l

printLog:
	@echo ================================ LOG ================================
	@cat $(LOG)
