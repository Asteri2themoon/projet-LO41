#include <environment.h>

#include <stdlib.h>
#include <stdio.h>
#include <sys/msg.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include <debug.h>
#include <message.h>
#include <linked_list.h>

typedef struct{
	Building *building;
	key_t key;
	key_t keyElevator;
	key_t keyPath;
	int msqid;
	int msqidElevator;
	int msqidPath;
	LinkedList *lst;
}Environment;

Environment *env=NULL;

void initEnvironment(char *path){
	env=(Environment*)malloc(sizeof(Environment));

	env->lst=initLst();
	env->key=ftok(path,IPC_MESSAGE_MOVE);
	env->keyElevator=ftok(path,IPC_MESSAGE_ELEVATOR);
	env->keyPath=ftok(path,IPC_MESSAGE_PATHFINDING);
	if(env->key==-1){
		debug("can't create a key");
		return;
	}
	if(env->keyElevator==-1){
		debug("can't create a key");
		return;
	}
	if(env->keyPath==-1){
		debug("can't create a key");
		return;
	}

	env->msqid=msgget(env->key,0666|IPC_CREAT|IPC_EXCL);
	if(env->msqid==-1){
		env->msqid=msgget(env->key,0666|IPC_CREAT);
		if(env->msqid==-1){
			debug("error with action's IPC (create/join)");
		}
	}

	env->msqidElevator=msgget(env->keyElevator,0666|IPC_CREAT|IPC_EXCL);
	if(env->msqidElevator==-1){
		env->msqidElevator=msgget(env->keyElevator,0666|IPC_CREAT);
		if(env->msqidElevator==-1){
			debug("error with elevator's IPC (create/join)");
		}
	}

	env->msqidPath=msgget(env->keyPath,0666|IPC_CREAT|IPC_EXCL);
	if(env->msqidPath==-1){
		env->msqidPath=msgget(env->keyPath,0666|IPC_CREAT);
		if(env->msqidPath==-1){
			debug("error with pathfinding's IPC (create/join)");
		}
	}
	int res;
	MsgBuf msg;//make the stack empty
	do{
		res=msgrcv(env->msqid,&msg,sizeof(msg.message),0,IPC_NOWAIT);
	}while(res>=0);
	do{
		res=msgrcv(env->msqidElevator,&msg,sizeof(msg.message),0,IPC_NOWAIT);
	}while(res>=0);
	do{
		res=msgrcv(env->msqidPath,&msg,sizeof(msg.message),0,IPC_NOWAIT);
	}while(res>=0);
	env->building=initBuilding();
}

void freeEnvironment(){
	int res=msgctl(env->msqid,IPC_RMID,NULL);
	if(res==-1){
		debug("error with IPC (destroy)");
	}
	res=msgctl(env->msqidElevator,IPC_RMID,NULL);
	if(res==-1){
		debug("error with IPC (destroy)");
	}

	freeBuilding(env->building);
	freeLst(env->lst);
	free(env);
	env=NULL;
}

void sendAction(People *p,unsigned int newRoom,unsigned int newFloor){
	Monitor m={condition:PTHREAD_COND_INITIALIZER,mutex:PTHREAD_MUTEX_INITIALIZER};
	MsgBuf msg;
	msg.type=1;
	sprintf(msg.message,"%ld,%d,%d,%ld",(long)p,newRoom,newFloor,(long)&m);
	msgsnd(env->msqid,&msg,sizeof(msg.message),0);

	pthread_mutex_lock(&m.mutex);
	pthread_cond_wait(&m.condition,&m.mutex);
	pthread_mutex_unlock(&m.mutex);
}

void sendElevatorStat(Elevator *e){
	Monitor m={condition:PTHREAD_COND_INITIALIZER,mutex:PTHREAD_MUTEX_INITIALIZER};
	MsgBuf msg;
	msg.type=1;
	sprintf(msg.message,"%ld,%ld",(long)e,(long)&m);
	msgsnd(env->msqidElevator,&msg,sizeof(msg.message),0);

	pthread_mutex_lock(&m.mutex);
	pthread_cond_wait(&m.condition,&m.mutex);
	pthread_mutex_unlock(&m.mutex);
}

void sendPathFinding(People *p){
	Monitor m={condition:PTHREAD_COND_INITIALIZER,mutex:PTHREAD_MUTEX_INITIALIZER};
	MsgBuf msg;
	msg.type=1;
	sprintf(msg.message,"%ld,%ld",(long)p,(long)&m);
	msgsnd(env->msqidElevator,&msg,sizeof(msg.message),0);

	pthread_mutex_lock(&m.mutex);
	pthread_cond_wait(&m.condition,&m.mutex);
	pthread_mutex_unlock(&m.mutex);
}

void getMessage(){
	MsgBuf msg;
	int res;
	char *token;
	int i;
	ActionRequest *req;
	Elevator *e;
	Monitor *m;
	People *p;
	do{//manage action request
		res=msgrcv(env->msqid,&msg,sizeof(msg.message),0,IPC_NOWAIT);
		if(res>=0){
			req=(ActionRequest*)malloc(sizeof(ActionRequest));
			token=strtok(msg.message,",");
			i=0;
			do{
				switch(i){
				case 0:
					req->p=(People*)atol(token);
					break;
				case 1:
					req->targetRoom=atoi(token);
					break;
				case 2:
					req->targetFloor=atoi(token);
					break;
				case 3:
					req->m=(Monitor*)atol(token);
					break;
				default:
					debug("warning: action request must be less long");
					break;
				};
				token=strtok(NULL,",");
				i++;
			}while(token!=NULL);
			if(i<4){
				debug("error: too few argument for an action request");
			}
			req->p->t0Wait=time(NULL);
			addLast(env->lst,(void*)req);
		}
	}while(res>=0);
	do{//manage elevator state update
		res=msgrcv(env->msqidElevator,&msg,sizeof(msg.message),0,IPC_NOWAIT);
		if(res>=0){
			token=strtok(msg.message,",");
			i=0;
			do{
				switch(i){
				case 0:
					e=(Elevator*)atol(token);
					break;
				case 1:
					m=(Monitor*)atol(token);
					break;
				default:
					debug("warning: elevator stat must be less long");
					break;
				};
				token=strtok(NULL,",");
				i++;
			}while(token!=NULL);
			if(i<2){
				debug("error: too few argument for an action request");
			}else{
				pthread_mutex_lock(&m->mutex);

				for(i=0;i<ELEVATOR_SIZE;i++){
					if(e->peoples[i]!=NULL){
						e->peoples[i]->position.floor=e->floor;
						printf("move %d %d %d\n",e->peoples[i]->id,e->floor,e->peoples[i]->position.room);
					}
				}

				while((req=popPeopleInRoom(e->floor,e->id,0))!=NULL){
					printf("targeted floor reached %d %d\n",e->floor,req->targetFloor);
					pthread_mutex_lock(&req->m->mutex);

					for(i=0;i<ELEVATOR_SIZE;i++){
						if(e->peoples[i]==req->p){
							e->peoples[i]=NULL;
							break;
						}
					}
					if(i==ELEVATOR_SIZE){
						debug("error: can't find people in an elevator");
					}

					req->p->position.floor=req->targetFloor;
					req->p->position.room=req->targetRoom;
					printf("move %d %d %d\n",req->p->id,req->p->position.floor,req->p->position.room);
					e->nbPeople--;

					pthread_cond_signal(&req->m->condition);
					pthread_mutex_unlock(&req->m->mutex);
				}

				while(e->nbPeople<e->capacity && (req=popPeopleInRoom(e->floor,0,e->id))!=NULL){
					pthread_mutex_lock(&req->m->mutex);

					addPeopleElevator(e,req->p);

					pthread_cond_signal(&req->m->condition);
					pthread_mutex_unlock(&req->m->mutex);

					printf("move %d %d %d\n",req->p->id,req->p->position.floor,req->p->position.room);
				}

				pthread_cond_signal(&m->condition);
				pthread_mutex_unlock(&m->mutex);
			}
		}
	}while(res>=0);
	do{//manage pathfinding request
		res=msgrcv(env->msqidPath,&msg,sizeof(msg.message),0,IPC_NOWAIT);
		if(res>=0){
			token=strtok(msg.message,",");
			i=0;
			do{
				switch(i){
				case 0:
					p=(People*)atol(token);
					break;
				case 1:
					m=(Monitor*)atol(token);
					break;
				default:
					debug("warning: action request must be less long");
					break;
				};
				token=strtok(NULL,",");
				i++;
			}while(token!=NULL);
			if(i<2){
				debug("error: too few argument for an action request");
			}else{
				pthread_mutex_lock(&m->mutex);
				p->path.length=AIPathFinding(p->path.path,p->position.floor,p->position.room,p->goal.position.floor,p->goal.position.room);
				pthread_cond_signal(&m->condition);
				pthread_mutex_unlock(&m->mutex);
			}
		}
	}while(res>=0);
}

void updatePosition(Elevator **tab){
	bool deleted=false;
	unsigned int cFloor,tFloor;
	int cRoom,tRoom;
	Element *e;
	ActionRequest *req;
	for(e=itFirst(env->lst);!isEnd(e);e=!deleted?itInc(e):itPop(env->lst,e)){
		deleted=false;
		req=(ActionRequest*)itGetData(e);
		pthread_mutex_lock(&req->m->mutex);
		tFloor=req->targetFloor;
		tRoom=req->targetRoom;
		if(req->p!=NULL){
			cFloor=req->p->position.floor;
			cRoom=req->p->position.room;
			if(cFloor>=0 && cFloor<BUILDING_SIZE){
				if(cFloor==tFloor && env->building->floor[tFloor]->nbRoom>tRoom){
					if(tRoom>=0){
						if(env->building->floor[tFloor]->adj[cRoom][tRoom]!=NOT_CONNECTED){
							req->p->position.floor=tFloor;
							req->p->position.room=tRoom;
							pthread_cond_signal(&req->m->condition);
							deleted=true;
							printf("move %d %d %d\n",req->p->id,req->p->position.floor,req->p->position.room);
							//debug("position updated");
						}
						//else debug("can't update position");
					}	
				}
				//else debug("can't update position");
			}
			else{
				debug("error: people cannot be in floor %d",cFloor);
			}
		}
		else{
			debug("error: NULL people");
		}
		pthread_mutex_unlock(&req->m->mutex);
	}
}

ActionRequest* popPeopleInRoom(unsigned int floor,int room,int targetElevator){
	Element *e;
	ActionRequest *req;
	for(e=itFirst(env->lst);!isEnd(e);e=itInc(e)){
		req=(ActionRequest*)itGetData(e);
		if(req->p!=NULL){
			if(req->p->position.floor==floor && req->targetFloor==floor && req->p->position.room==room && req->targetRoom==targetElevator){
				itPop(env->lst,e);
				return req;
			}
		}
	}
	return NULL;
}

unsigned int getTarget(Elevator *el){
	/*if(el->floor==0)
		return BUILDING_SIZE-1;
	return 0;*/
	Element *e;
	ActionRequest *req;
	int min=time(NULL)+1000;
	unsigned int minFloor=0;
	for(e=itFirst(env->lst);!isEnd(e);e=itInc(e)){
		req=(ActionRequest*)itGetData(e);
		if(req->p!=NULL){
			if(req->targetRoom==el->id || (req->p->position.room==el->id)){
				if(req->targetFloor<BUILDING_SIZE && req->p->t0Wait<min){
					minFloor=req->targetFloor;
					min=req->p->t0Wait;
				}
			}
		}
	}
	return minFloor;
}

int getSmallestNeighbour(unsigned int floor,int room){
	int i,min=0;
	if(room>=0){
		for(i=1;i<env->building->floor[floor]->nbRoom;i++){
			if(env->building->floor[floor]->adj[room][i]<env->building->floor[floor]->adj[room][min]){
				min=i;
			}
		}
		return min;
	}
	return 0;
}



int AIPathFinding(Position *lst,unsigned int cFloor,int cRoom,unsigned int tFloor,int tRoom){
	int p=0,dec;
	int room=cRoom;
	Position tmp;
	if(cFloor==tFloor){
		tmp.floor=cFloor;
		tmp.room=room;
		lst[p++]=tmp;
		while(room!=0){
			room=getSmallestNeighbour(cFloor,room);
			tmp.room=room;
			lst[p++]=tmp;
		}
		dec=env->building->floor[tFloor]->dist[tRoom];
		p+=dec-1;
		dec=p;
		room=tRoom;
		tmp.room=tRoom;
		lst[dec--]=tmp;
		while(room!=0){
			room=getSmallestNeighbour(tFloor,room);
			tmp.room=room;
			lst[dec--]=tmp;
		}
	}else{
		tmp.floor=cFloor;
		tmp.room=room;
		lst[p++]=tmp;
		while(room!=0){
			room=getSmallestNeighbour(cFloor,room);
			tmp.room=room;
			lst[p++]=tmp;
		}
		tmp.room=(rand()%3)-3;
		lst[p++]=tmp;
		dec=env->building->floor[tFloor]->dist[tRoom];
		p+=dec;
		dec=p;
		room=tRoom;
		tmp.floor=tFloor;
		tmp.room=tRoom;
		lst[dec--]=tmp;
		while(room!=0){
			room=getSmallestNeighbour(tFloor,room);
			tmp.room=room;
			lst[dec--]=tmp;
		}
	}
	return p+1;
}

void updatePathOfClosestTech(Elevator *el){
	Element *e;
	People *p,*min=NULL;
	ActionRequest *a;
	int minDist=BUILDING_SIZE;
	for(e=itFirst(env->building->people);!isEnd(e);e=itInc(e)){
		p=(People*)itGetData(e);
		if(p->type==TECHNICIAN && minDist>abs(p->position.floor-el->floor)){
			minDist=abs(p->position.floor-el->floor);
			min=p;
		}
	}
	if(min!=NULL){
		min->path.length=AIPathFinding(min->path.path,min->position.floor,min->position.room,min->position.floor,el->id);
		min->goal.elevator=el;
	}
	for(e=itFirst(env->lst);!isEnd(e);e=itInc(e)){
		a=(ActionRequest*)itGetData(e);
		if(a->p==min){
			itPop(env->lst,e);
			pthread_mutex_lock(&a->m->mutex);
			pthread_cond_signal(&a->m->condition);
			pthread_mutex_unlock(&a->m->mutex);
			break;
		}
	}
}

Goal randomGoalHost(){
	int size=getLength(env->building->people),i,random;
	Element *e;
	People *p;
	Goal g;
	do{
		random=rand()%size;
		i=0;
		for(e=itFirst(env->building->people);(!isEnd(e)) && i<random;e=itInc(e)){
			i++;
		}
		p=itGetData(e);
	}while(p->type!=HOST);
	g.position=p->position;
	return g;
}
