import sys
import curses
from curses import wrapper

stdscr=curses.initscr()

def main(stdscr):
	people={}
	elevator=[1,1,1]
	elevatorCapa=[0,0,0]
	elevatorFlag=['','','']
	height=30
	hspan=40
	
	stdscr.clear()

	for line in sys.stdin:	
		args=line.split(" ")
		if len(args)>=2:
			if args[0] == 'leave':
				people.pop(int(args[1]),None)
			elif args[0] == 'break':
				elevatorFlag[int(args[1])-1]='/!\\'
			elif args[0] == 'fix':
				elevatorFlag[int(args[1])-1]=''
		if len(args)>=4:
			if args[0] == 'elevator':
				elevator[int(args[1])-1]=int(args[2])
				elevatorCapa[int(args[1])-1]=int(args[3])
			if args[0] == 'move':
				people[int(args[1])]['floor']=int(args[2])+1
				people[int(args[1])]['room']=int(args[3])
		if len(args)>=5:
			if args[0] == 'new':
				if args[1] == 'host':
					people[int(args[2])]={'type':'host','floor':int(args[3])+1,'room':int(args[4])}
				if args[1] == 'technician':
					people[int(args[2])]={'type':'technician','floor':int(args[3])+1,'room':int(args[4])}
				if args[1] == 'deliveryman':
					people[int(args[2])]={'type':'deliveryman','floor':int(args[3])+1,'room':int(args[4])}
				if args[1] == 'visitor':
					people[int(args[2])]={'type':'visitor','floor':int(args[3])+1,'room':int(args[4])}
	
		stdscr.clear()
		for i,f in enumerate(elevator):
			stdscr.addstr(i,0,'elevator {} floor:{} people:{}  {}'.format(i+1,f,elevatorCapa[i],elevatorFlag[i]))
		i=4
		for key,value in people.items():
			stdscr.addstr(i%height,(i//height)*hspan,'{} id:{} floor:{} room:{}'.format(value['type'],key,value['floor'],'a{}'.format(-value['room']) if value['room']<0 else value['room']))
			i+=1
		stdscr.refresh()

wrapper(main)
