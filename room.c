#include <room.h>

#include <stdlib.h>
#include <stdio.h>

#include <debug.h>

Room* initRoom(RoomType type,unsigned int c,unsigned int elevator){
	unsigned int i;
	Room *r=(Room*)malloc(sizeof(Room));
	r->type=type;
	r->capacity=c;
	r->nbPeople=0;
	for(i=0;i<ELEVATOR_COUNT;i++) r->elevator[i]=elevator&(1<<i);
	return r;
}

void freeRoom(Room *r){
	free(r);
}

RoomType randRoomType(){
	switch(rand()%2){
	case 0:
		return TECHROOM;
	case 1:
		return OFFICE;
	default:
		debug("error in %s\n",__FILE__);
		return OFFICE;
	}
	return OFFICE;
}

char* roomType2Str(RoomType type){
	switch(type){
	case 0:
		return "corridor";
	case 1:
		return "office";
	case 2:
		return "hall";
	case 3:
		return "tech room";
	default:
		debug("error in %s\n",__FILE__);
		return "office";
	}
	return "office";
}
