#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#define MESSAGE_MAX_LENGTH 128

typedef struct{
	long type;
	char message[MESSAGE_MAX_LENGTH];
}MsgBuf;

#endif
