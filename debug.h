#ifndef __HEADER_DEBUG__
#define __HEADER_DEBUG__

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

clock_t t0;
FILE *logFile;
char debugBuffer[128];

#define debug(msg,...) sprintf(debugBuffer,msg,##__VA_ARGS__);\
			fprintf(stderr,"[%f]pid=%d,file=%s,function=%s,line=%d:%s\n",(double)(clock()),getpid(),__FILE__,__FUNCTION__,__LINE__,debugBuffer)

#ifdef LOG_FILE
void debug_init(){
	t0=clock();
	logFile=fopen(LOG_FILE,"w+");
	if(logFile!=0){
		dup2(fileno(logFile),STDERR_FILENO);
	}else{
		debug("can't create log file");
	}
}
#else
#define debug_init() t0=clock();
#endif

#ifdef LOG_FILE
#define debug_exit() if(logFile!=0)fclose(logFile)
#else
#define debug_exit()
#endif

#endif
