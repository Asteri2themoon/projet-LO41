#ifndef __BUILDING_H__
#define __BUILDING_H__

#include <floor.h>
#include <linked_list.h>

#define BUILDING_SIZE 25

typedef struct{
	Floor *floor[BUILDING_SIZE];
	LinkedList *people;
}Building;

Building* initBuilding();
void freeBuilding(Building *building);

void debugBuilding(Building *b);

#endif
