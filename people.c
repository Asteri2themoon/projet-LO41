#include <people.h>

#include <stdlib.h>
#include <stdio.h>

#include <debug.h>
#include <environment.h>



People* initPeople(Position position,PeopleType type, Goal goal, Inventory inventory){
	static int id=1;
	People *people= (People*)malloc(sizeof(People));
	if(type == VISITOR || type == DELIVERYMAN){
		position.floor = 0;
		position.room = 0;
	}
	people->id=id++;
	people->t0Wait=0;
	people->position = position;
	people->type = type;
	people->goal = goal;
	people->inventory = inventory;
	people->path.length=-1;
	people->path.pos=0;
	return people;
}

void freePeople(People* people){
	if(people){
		free(people);
	}
}

void* threadPeople(void *arg){
	People *p=(People*)arg;
	
	switch(p->type){
		case VISITOR:
		if(p->path.length==-1){
			p->path.length=AIPathFinding(p->path.path,p->position.floor,p->position.room,p->goal.position.floor,p->goal.position.room);
			//sendPathFinding(p);
		}
		p->path.pos=1;
		while(p->path.pos<p->path.length){
			sendAction(p,p->path.path[p->path.pos].room,p->path.path[p->path.pos].floor);
			p->path.pos++;
		}
		sleep(rand()%20);
		p->path.pos-=2;
		while(p->path.pos>=0){
			if(p->path.path[p->path.pos].room<0){
				//printf("edit floor\n");
				p->path.path[p->path.pos].floor=p->path.path[p->path.pos+1].floor;
			}
			sendAction(p,p->path.path[p->path.pos].room,p->path.path[p->path.pos].floor);
			p->path.pos--;
		}
		printf("leave %d\n",p->id);
	break;
		case HOST:
		while(1)sleep(1);
	break;
		case DELIVERYMAN:
		if(p->path.length==-1){
			p->path.length=AIPathFinding(p->path.path,p->position.floor,p->position.room,p->goal.position.floor,p->goal.position.room);
		}
		p->path.pos=1;
		while(p->path.pos<p->path.length){
			sendAction(p,p->path.path[p->path.pos].room,p->path.path[p->path.pos].floor);
			p->path.pos++;
		}
		p->path.pos-=2;
		while(p->path.pos>=0){
			if(p->path.path[p->path.pos].room<0){
				p->path.path[p->path.pos].floor=p->path.path[p->path.pos+1].floor;
			}
			sendAction(p,p->path.path[p->path.pos].room,p->path.path[p->path.pos].floor);
			p->path.pos--;
		}
		printf("leave %d\n",p->id);
	break;
		case TECHNICIAN:
		while(1){
			sendAction(p,-4,p->position.floor);
			sleep(5);
			fixElevator(p->goal.elevator);
		}
	break;
		default:
		debug("error: unknown type of people");
	break;
	};

	pthread_exit(NULL);
}
