#ifndef __FLOOR_H__
#define __FLOOR_H__

#include <room.h>

#define NOT_CONNECTED -1

typedef struct{
	unsigned int nbRoom;
	Room **rooms;
	unsigned int *dist;
	unsigned int **adj;
}Floor;

Floor* initFloor(unsigned int count);
Floor* initHall();
void freeFloor(Floor *f);

#endif
