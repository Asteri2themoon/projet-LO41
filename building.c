#include <building.h>

#include <stdlib.h>
#include <stdio.h>

#include <debug.h>
#include <people.h>
#include <config.h>

Building* initBuilding(){
	unsigned int i,j;
	Building* b=(Building*)malloc(sizeof(Building));
	b->floor[0]=initHall();
	People *p;
	Goal g;
	Inventory inv;
	Position pos;
	b->people=initLst();
	for(i=1;i<BUILDING_SIZE;i++){
		b->floor[i]=initFloor(rand()%(MAX_FLOOR_SIZE-MIN_FLOOR_SIZE+1)+MIN_FLOOR_SIZE);
		for(j=0;j<b->floor[i]->nbRoom;j++){
			if(b->floor[i]->rooms[j]->type==OFFICE){
				if((rand()%100)<PERC_HOST){
					pos.floor=i;
					pos.room=j;
					p=initPeople(pos,HOST,g,inv);
					printf("new host %d %d %d\n",p->id,i,j);
					addLast(b->people,p);
					if(pthread_create(&p->thread,NULL,threadPeople,p)==-1){
						debug("error: can't create new thread");
					}
				}
			}else if(b->floor[i]->rooms[j]->type==TECHROOM){
				if((rand()%100)<PERC_TECH){
					pos.floor=i;
					pos.room=j;
					p=initPeople(pos,TECHNICIAN,g,inv);
					printf("new technician %d %d %d\n",p->id,i,j);
					addLast(b->people,p);
					if(pthread_create(&p->thread,NULL,threadPeople,p)==-1){
						debug("error: can't create new thread");
					}
				}
			}
		}
	}
	return b;
}

void freeBuilding(Building *building){
	unsigned int i;
	Element *e;
	for(i=0;i<BUILDING_SIZE;i++){
		freeFloor(building->floor[i]);
	}
	for(e=itFirst(building->people);!isEnd(e);e=itInc(e)){
		free((People*)itGetData(e));
	}
	freeLst(building->people);
	free(building);
}

void debugBuilding(Building *b){
	unsigned int i,j,k;
	char tmp[64];
	debug("building");
	for(i=0;i<BUILDING_SIZE;i++){
		debug("\tfloor %d",i);
		for(j=0;j<b->floor[i]->nbRoom;j++){
			debug("\t\troom %d",j);
			debug("\t\t\ttype:%s",roomType2Str(b->floor[i]->rooms[j]->type));
			debug("\t\t\tdistance:%d",b->floor[i]->dist[j]);
			debug("\t\t\tcapacity:%d",b->floor[i]->rooms[j]->capacity);
			debug("\t\t\tnbPeople:%d",b->floor[i]->rooms[j]->nbPeople);
			debug("\t\t\televator: 0=%s, 1=%s 2=%s",b->floor[i]->rooms[j]->elevator[0] ? "true" : "false",b->floor[i]->rooms[j]->elevator[1] ? "true" : "false",b->floor[i]->rooms[j]->elevator[2] ? "true" : "false");
		}
		debug("\t\tadj:");
		for(j=0;j<b->floor[i]->nbRoom;j++){
			for(k=0;k<b->floor[i]->nbRoom;k++){
				tmp[k<<1]=b->floor[i]->adj[j][k]==NOT_CONNECTED?'#':'1';
				tmp[(k<<1)+1]=' ';
			}
			tmp[k<<1]='\0';
			debug("\t\t\t%s",tmp);
		}
	}
}
