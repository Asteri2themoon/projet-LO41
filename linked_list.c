#include <linked_list.h>
#include <debug.h>
#include <stdlib.h>
#include <stdio.h>

LinkedList* initLst(){
	LinkedList *lst=(LinkedList*)malloc(sizeof(LinkedList));
	
	lst->length=0;
	lst->first=NULL;
	lst->last=NULL;

	return lst;
}

void addFirst(LinkedList *lst,void *data){
	Element *new;
	if(lst==NULL){
		debug("error: try to add data on a null linked list");
		return;
	}
	new=(Element*)malloc(sizeof(Element));
	if(new==NULL){
		debug("error: can't allocate more memory for the linked list");
		return;
	}
	new->data=data;
	new->prev=NULL;
	if(lst->first==NULL){
		new->next=NULL;
		lst->last=new;
	}
	else{
		new->next=lst->first;
		new->next->prev=new;
	}
	lst->length++;
	lst->first=new;
}

void addLast(LinkedList *lst,void *data){
	Element *new;
	if(lst==NULL){
		debug("error: try to add data on a null linked list");
		return;
	}
	new=(Element*)malloc(sizeof(Element));
	if(new==NULL){
		debug("error: can't allocate more memory for the linked list");
		return;
	}
	new->data=data;
	new->next=NULL;
	if(lst->last==NULL){
		new->prev=NULL;
		lst->first=new;
	}
	else{
		new->prev=lst->last;
		new->prev->next=new;
	}
	lst->length++;
	lst->last=new;
}

void* popFirst(LinkedList *lst){
	void *data=NULL;
	if(lst==NULL){
		debug("error: try to pop data on a null linked list");
		return NULL;
	}
	if(lst->first!=NULL){
		data=lst->first->data;
		if(lst->first==lst->last){
			free(lst->first);
			lst->first=NULL;
			lst->last=NULL;
		}else{
			lst->first=lst->first->next;
			free(lst->first->prev);
			lst->first->prev=NULL;
		}
	}
	lst->length--;
	return data;
}

void* popLast(LinkedList *lst){
	void *data=NULL;
	if(lst==NULL){
		debug("error: try to pop data on a null linked list");
		return NULL;
	}
	if(lst->last!=NULL){
		data=lst->last->data;
		if(lst->first==lst->last){
			free(lst->first);
			lst->first=NULL;
			lst->last=NULL;
		}else{
			lst->last=lst->last->prev;
			free(lst->last->next);
			lst->last->next=NULL;
		}
	}
	lst->length--;
	return data;
}

void* getFirst(LinkedList *lst){
	if(lst!=NULL && lst->first!=NULL){
		return lst->first->data;
	}
	return NULL;
}

void* getLast(LinkedList *lst){
	if(lst!=NULL && lst->last!=NULL){
		return lst->last->data;
	}
	return NULL;
}

unsigned int getLength(LinkedList *lst){
	if(lst!=NULL){
		return lst->length;
	}
	return 0;
}

void freeLst(LinkedList *lst){
	while(getLength(lst)>0){
		popFirst(lst);
	}
	free(lst);
}

Element* itFirst(LinkedList *lst){
	if(lst!=NULL){
		return lst->first;
	}
	return NULL;
}

Element* itLast(LinkedList *lst){
	if(lst!=NULL){
		return lst->last;
	}
	return NULL;
}

Element* itInc(Element* e){
	if(e!=NULL){
		return e->next;
	}
	return NULL;
}

Element* itDec(Element* e){
	if(e!=NULL){
		return e->prev;
	}
	return NULL;
}

bool isEnd(Element *e){
	return e==NULL;
}

void* itGetData(Element *e){
	if(e!=NULL){
		return e->data;
	}
	return NULL;
}

Element* itPop(LinkedList *lst,Element *e){
	Element *tmp;
	if(lst==NULL || e==NULL){
		return NULL;
	}
	if(lst->first==e){
		popFirst(lst);
		return lst->first;
	}else if(lst->last==e){
		popLast(lst);
		return lst->last;
	}
	tmp=e;
	e->prev->next=e->next;
	e->next->prev=e->prev;
	e=e->next;
	free(tmp);
	lst->length--;
	return e;
}
