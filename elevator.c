#include <elevator.h>
#include <people.h>

#include <stdlib.h>
#include <stdio.h>

#include <debug.h>
#include <building.h>
#include <environment.h>

Elevator* initElevator(int id){
	Elevator* elevator = (Elevator*)malloc(sizeof(Elevator));
	elevator->capacity = ELEVATOR_SIZE;
	//elevator->mutex = PTHREAD_MUTEX_INITIALIZER;
	elevator->nbPeople = 0;
	elevator->floor = 0;
	elevator->targetedFloor = 0;
	elevator->state = ELEVATOR_RUNNING_STATE;
	elevator->id=id;
	return elevator;
}

void* runElevator(void *arg){
	Elevator* elevator = (Elevator*) arg;
	unsigned int newTarget;
	while(1){
	//while(elevator->floor!=elevator->targetedFloor){
		if(elevator->floor<elevator->targetedFloor){
			elevatorUp(elevator);
		}else if(elevator->floor>elevator->targetedFloor){
			elevatorDown(elevator);
		}else{
			newTarget=getTarget(elevator);
			if(newTarget<BUILDING_SIZE){
				elevator->targetedFloor=newTarget;
			}
		}
		sendElevatorStat(elevator);
	}
	pthread_exit(NULL);
}

void elevatorUp(Elevator *elevator){
	if(elevator){
		if(elevator->state == ELEVATOR_RUNNING_STATE){
			if(elevator->floor < (BUILDING_SIZE-1) ){
				printf("elevator %d %d %d\n",-elevator->id, (elevator->floor)+1,elevator->nbPeople);
				sleep(ELEVATOR_MOVE_TIME);
				elevator->floor++;
				// Add a notification to people inside ??

			}else{
				printf("Elevator can't move up, already on the last floor\n");
			}
		}else{
			printf("Elevator can't move, state: ");
			printElevatorState(elevator);
		}
	}else{
		debug("Null Pointer given to elevatorUp");
	}
}

void elevatorDown(Elevator *elevator){
	if(elevator){
		if(elevator->state == ELEVATOR_RUNNING_STATE){
			if(elevator->floor > 0 ){
				printf("elevator %d %d %d\n",-elevator->id, (elevator->floor)+1,elevator->nbPeople);
				sleep(ELEVATOR_MOVE_TIME);
				elevator->floor--;
				// Add a notification to people inside ??

			}else{
				printf("Elevator can't move up, already on the first floor\n");
			}
		}else{
			printf("Elevator can't move, state: ");
			printElevatorState(elevator);
		}
	}else{
		debug("Null Pointer given to elevatorUp");
	}
}

void printElevatorState(Elevator *elevator){
	if(elevator){
		switch(elevator->state){
			case ELEVATOR_RUNNING_STATE: printf("Elevator Running Normally\n");
			break;
			case ELEVATOR_BREAKDOWN_TYPE_1: printf("Elevator BreakDown Type 1\n");
			break;
			case ELEVATOR_BREAKDOWN_TYPE_2: printf("Elevator BreakDown Type 2\n");
			break;
			case ELEVATOR_BREAKDOWN_TYPE_3: printf("Elevator BreakDown Type 3\n");
			default:
				printf("error with elevator\n");
				break;
		}
	}else{
		debug("Null Pointer given to printElevatorState");
	}
}

void freeElevator(Elevator *elevator){
	unsigned int i;
	if(elevator){
		for(i=0;i<elevator->capacity;i++){
			if(elevator->peoples[i]){
				freePeople(elevator->peoples[i]);
			}
		}
		free(elevator->peoples);
		free(elevator);
	}else{
		debug("Null Pointer given to freeElevator");
	}
	
}

void addPeopleElevator(Elevator *e,People *p){
	int i;
	if(e==NULL || p==NULL){
		debug("error: can't add people in an elevator with NULL pointeur");
		return;
	}
	if(e->capacity>e->nbPeople){
		for(i=0;i<ELEVATOR_SIZE;i++){
			if(e->peoples[i]==NULL){
				break;
			}
		}
		if(i==ELEVATOR_SIZE){
			debug("error: wrong capacity on an elevator");
			e->nbPeople=e->capacity;
		}else{
			e->peoples[i]=p;
			p->position.room=e->id;
			e->nbPeople++;
		}
	}else{
		debug("error: can't add people in an full elevator");
	}
}

void breakElevator(Elevator *e){
	switch(rand()%3){
	case 0:
		e->state=ELEVATOR_BREAKDOWN_TYPE_1;
		break;
	case 1:
		e->state=ELEVATOR_BREAKDOWN_TYPE_2;
		break;
	case 2:
		e->state=ELEVATOR_BREAKDOWN_TYPE_3;
		break;
	default:
		debug("error: impossible random numbre");
		break;
	}
	printf("break %d\n",-e->id);
}

void fixElevator(Elevator *e){
	e->state=ELEVATOR_RUNNING_STATE;
	printf("fix %d\n",-e->id);
}
