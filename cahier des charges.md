# projet de LO41
# threads
- IA
- ascenseurs
- personnes

# structure de données
## IA
- le bâtiment
- un arbre de recherche contenant des tubes vers toutes les personnes (la clé est le pid?)
- nombre de borne
- nombre de personne utilisant les bornes (blocage de la requête de path finding)
- tableau des outils (mémoire partagé avec les techniciens) protégé par un mutex

## bâtiment
- tableau d'étages

## étage
- tableau des pièces
- matrice d'adjacence

## pièces
- type du pièce (enum)
- capacité du nœud
- nombre de personne dans le nœud
- tableau de taille 3 du tube vers l'ascenseur (-1 si il n'y a pas de connexion avec l'ascenseur i dans cette pièces)

## type de pièce (enum)
 - couloir
 - bureau
 - hall d'entré (accès au borne pour contacter l'ia)
 - local technique (pour ranger les techniciens quand ils n'ont rien à faire)

## personne
- position
- type de personne (enum)
- état/objectif

> union
> - pointeur vers un tableau des outils (mémoire partagé avec l'ia) protégé par un mutex
> - truc à livrer (liste chaîné de string?)

## position
- numéro de l'étage
- numéro de la pièce (position dans le tableau des pièces de l'étage)

## type personne (enum)
- technicien
- résident
- livreur
- visiteur

## objectif
- position de l'objectif (NULL si encore inconnue)
- liste chaîné des positions intermédiaire
- type de personne (enum)

> union
> - pid de la personne à atteindre
> - numéro de l'ascenseur à réparer

## requête de déplacement
- réponse (booléen)
- pid de la personne
- position actuelle (maj dans la réponse de l'ia)
- position désiré (pièces à -1, -2 et -3 pour indiquer un ascenseur???)

## requête de path finding
- réponse (booléen)
- pid de la personne
- position actuelle
- objectif
- taille du tableau des positions intermédiaires (initialisé à 0 pour la requête)
- tableau des positions intermédiaires (initialisé à NULL pour la requête)

## ascenseur
- position actuelle
- tableau de pile fifo (sous forme le liste chaîné?) des pid des personnes (une pile par étage)
- capacité de l'ascenseur
- tableau de pid des personnes dans l'ascenseur
- état (enum: marche, arrêt ou veille)

**sémaphore pour autoriser l'accès à l'ascenceur**

## requête indiquant l'étage
- réponse (booléen) (requête: ascenseur -> IA -> personne, réponse: personne -> IA -> ascenseur)
- étage actuelle
- sortie de la personne (initialisé à faux)

# partage des tâches

## arthur
- batiment, étages et pièces
- communication avec les tubes
- IA (com+arbre)
- path finding

## vincent
- ascenseurs
- personnes
- IA (mémoire partagé)

