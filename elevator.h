#ifndef __ELEVATOR_H__
#define __ELEVATOR_H__

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>


#include <building.h>
#include <people.h>

 /*** DEFINES ***/
#define ELEVATOR_COUNT 3
#define ELEVATOR_SIZE 5
// Elevator States (0 if running normally)
#define ELEVATOR_RUNNING_STATE 0x0
#define ELEVATOR_BREAKDOWN_TYPE_1 0x1
#define ELEVATOR_BREAKDOWN_TYPE_2 0x2
#define ELEVATOR_BREAKDOWN_TYPE_3 0x4
#define ELEVATOR_MOVE_TIME 1

struct _people;

 /*** STRUCTS ***/
// Elevator Struct
struct _elevator{
	unsigned int capacity;
	int id;
	unsigned int nbPeople;
	unsigned int floor;
	unsigned int state;
	unsigned int targetedFloor;
	struct _people *peoples[ELEVATOR_SIZE];
	pthread_t thread;
	//pthread_mutex_t mutex;
};

typedef struct _elevator Elevator;

 /*** FUNCTIONS ***/
// Elevator initialization
Elevator* initElevator(int id);
// Thread Routine of an Elevator, please give an Elevator in parameters
void* runElevator(void *arg);
// Move the Elevator Up
void elevatorUp(Elevator *elevator);
// Move the Elevator Down
void elevatorDown(Elevator *elevator);
// Print the state of the Elevator
void printElevatorState(Elevator *elevator);
// Free The Elevator Struct
void freeElevator(Elevator *elevator);

void addPeopleElevator(Elevator *e,struct _people *p);

void breakElevator(Elevator *e);
void fixElevator(Elevator *e);

#endif
