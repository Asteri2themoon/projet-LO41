#ifndef __PEOPLE_H__
#define __PEOPLE_H__

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include <config.h>
#include <elevator.h>

struct _people;
struct _elevator;

 /*** ENUMS-UNIONS-STRUCTS ***/
typedef enum{
	VISITOR,
	HOST,
	DELIVERYMAN,
	TECHNICIAN
}PeopleType;

typedef enum{
	TYPE_1,
	TYPE_2,
	TYPE_3
}ToolType;

typedef union{
	ToolType tool[3];
	char *deliverable;
}Inventory;

typedef struct{
	unsigned int floor;
	int room; // Put -1 to -3 if in an elevator
}Position;

typedef union {
	Position position; // A DeliveryMan or a Host wants to go at a position
	struct _elevator *elevator; // A Technician wants to go repair an Elevator
}Goal;

typedef struct{
	Position path[(MAX_FLOOR_SIZE+1)*2];
	int length;
	int pos;
}Path;

struct _people{
	int id;
	pthread_t thread;
	Position position;
	PeopleType type;
	Inventory inventory;
	Goal goal;
	unsigned int t0Wait;
	Path path;
};

typedef struct _people People;

 /*** FUNCTIONS ***/
// People Initialization
People* initPeople(Position position,PeopleType type, Goal goal, Inventory inventory);
// Free the people structure
void freePeople(People* people);

void* threadPeople(void *arg);

#endif
