#include <stdlib.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define LOG_FILE "log.txt"

#include <debug.h>
#include <building.h>
#include <message.h>
#include <environment.h>
#include <linked_list.h>
#include <elevator.h>

int main(int argc,char *argv[]){
	srand(15998456);

	debug_init();
	
	LinkedList *tmpPeople=initLst();

	initEnvironment(argv[0]);

	Elevator *elevator[3];
	int i;
	for(i=0;i<ELEVATOR_COUNT;i++){
		elevator[i]=initElevator(-1-i);
		elevator[i]->floor=0;
		if(pthread_create(&elevator[i]->thread,NULL,runElevator,elevator[i])==-1){
			debug("can't start new thread for elevator");
		}
	}
	
	int tn=time(NULL)+TOTAL_LENGTH;
	while(time(NULL)<tn){
		if((rand()%100)<PERC_RANDOM_DELIVERYMAN){
			Position p={floor:0,room:0};
			Goal g=randomGoalHost();
			Inventory inv;
			People *newP=initPeople(p,DELIVERYMAN,g,inv);
			addLast(tmpPeople,newP);
			if(pthread_create(&newP->thread,NULL,threadPeople,newP)==-1){
				debug("error: can't create new thread");
			}else{
				printf("new deliveryman %d %d %d\n",newP->id,p.floor,p.room);
			}
		}
		if((rand()%100)<PERC_RANDOM_VISITOR){
			Position p={floor:0,room:0};
			Goal g=randomGoalHost();
			Inventory inv;
			People *newP=initPeople(p,VISITOR,g,inv);
			addLast(tmpPeople,newP);
			if(pthread_create(&newP->thread,NULL,threadPeople,newP)==-1){
				debug("error: can't create new thread");
			}else{
				printf("new visitor %d %d %d\n",newP->id,p.floor,p.room);
			}
		}
		if((rand()%1000)<PERM_RANDOM_ELEVATOR_BREAK){
			int elev=rand()%3;
			breakElevator(elevator[elev]);
			updatePathOfClosestTech(elevator[elev]);
		}
		getMessage();
		sleep(1);
		updatePosition(elevator);
		fflush(stdout);
	}

	for(i=0;i<ELEVATOR_COUNT;i++){
		freeElevator(elevator[i]);
	}
	freeEnvironment();

	debug_exit();
	return 0;
}
