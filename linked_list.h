#ifndef __LINKED_LIST_H__
#define __LINKED_LIST_H__

#include <stdbool.h>

typedef struct element{
	struct element *next;
	struct element *prev;
	void *data;
}Element;

typedef struct{
	Element *first;
	Element *last;
	unsigned int length;
}LinkedList;

LinkedList* initLst();

void addFirst(LinkedList *lst,void *data);
void addLast(LinkedList *lst,void *data);

void* popFirst(LinkedList *lst);
void* popLast(LinkedList *lst);

void* getFirst(LinkedList *lst);
void* getLast(LinkedList *lst);

unsigned int getLength(LinkedList *lst);

void freeLst(LinkedList *lst);

Element* itFirst(LinkedList *lst);
Element* itLast(LinkedList *lst);
Element* itInc(Element *e);
Element* itDec(Element *e);
bool isEnd(Element *e);
void* itGetData(Element *e);

Element* itPop(LinkedList *lst,Element *e);

#endif
