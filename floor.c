#include <floor.h>

#include <stdlib.h>
#include <stdio.h>

#include <config.h>

void initRoomDist(Floor *f,int room,int n){
	int i;
	if(room<f->nbRoom && n<f->dist[room]){
		f->dist[room]=n;
	}else{
		return;
	}
	for(i=0;i<f->nbRoom;i++){
		if(f->adj[room][i]!=NOT_CONNECTED){
			initRoomDist(f,i,n+1);
		}
	}
}

Floor* initFloor(unsigned int count){
	unsigned int i,j,corrid=0;
	Floor *f=(Floor*)malloc(sizeof(Floor));
	f->nbRoom=count;
	f->rooms=(Room**)malloc(sizeof(Room*)*count);
	corrid=rand()%(MAX_CORRIDOR-MIN_CORRIDOR+1)+MIN_CORRIDOR;
	f->rooms[0]=initRoom(CORRIDOR,rand()%(MAX_ROOM-MIN_ROOM+1)+MIN_ROOM,ELEVATOR_0|ELEVATOR_1|ELEVATOR_2);
	for(i=1;i<count;i++){
		if(i<corrid){
			f->rooms[i]=initRoom(CORRIDOR,rand()%(MAX_ROOM-MIN_ROOM+1)+MIN_ROOM,NO_ELEVATOR);
		}else{
			f->rooms[i]=initRoom(randRoomType(),rand()%(MAX_ROOM-MIN_ROOM+1)+MIN_ROOM,NO_ELEVATOR);
		}
	}
	f->adj=(unsigned int**)malloc(sizeof(unsigned int*)*count);
	for(i=0;i<count;i++){
		f->adj[i]=(unsigned int*)malloc(sizeof(unsigned int)*count);
		for(j=0;j<count;j++){
			f->adj[i][j]=NOT_CONNECTED;
		}
	}
	for(i=0;i<corrid && (i+1)<count;i++){
		f->adj[i][i+1]=1;
		f->adj[i+1][i]=1;
	}
	for(;i<count;i++){
		j=rand()%corrid;
		f->adj[i][j]=1;
		f->adj[j][i]=1;
	}
	f->dist=(unsigned int*)malloc(sizeof(unsigned int)*count);
	for(i=0;i<count;i++){
		f->dist[i]=0xffffffff;
	}
	initRoomDist(f,0,0);
	return f;
}

Floor* initHall(){
	Floor *f=(Floor*)malloc(sizeof(Floor));
	f->nbRoom=1;
	f->rooms=(Room**)malloc(sizeof(Room*));
	f->rooms[0]=initRoom(CORRIDOR,rand()%(MAX_ROOM-MIN_ROOM+1)+MIN_ROOM,ELEVATOR_0|ELEVATOR_1|ELEVATOR_2);
	f->adj=(unsigned int**)malloc(sizeof(unsigned int*));
	f->adj[0]=(unsigned int*)malloc(sizeof(unsigned int));
	f->adj[0][0]=NOT_CONNECTED;
	f->dist=(unsigned int*)malloc(sizeof(unsigned int));
	f->dist[0]=0;
	return f;
}
void freeFloor(Floor *f){
	unsigned int i;
	for(i=0;i<f->nbRoom;i++){
		free(f->adj[i]);
		free(f->rooms[i]);
	}
	free(f->adj);
	free(f->rooms);
	free(f->dist);
	free(f);
}
